import React, { Component } from 'react'
import { Table, Button, Row, Container, Spinner } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export class Products extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        }
        this.fetchData = this.fetchData.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
    }

    fetchData() {
        fetch("api/product")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: false,
                        error
                    }, () => toast.error(`Ошибка: ${this.state.error.message}`));
                }
            );
    }

    componentDidMount() {
        this.fetchData();
    }

    deleteItem(e) {
        const conf = window.confirm(`Вы точно хотите удалить продукт?`);
        let id = parseFloat(e.target.parentNode.getAttribute("data-key"));
        if (conf) {
            this.deleteProduct(id);
        }
    }

    deleteProduct(id) {

        const requestUri = "api/product/" + id;

        const requestMetadata = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        fetch(requestUri, requestMetadata)
            .then((res) => {
                if (res.status == 204) {
                    toast.success('Продукт удалён!');
                }
                else if (res.status == 404)
                    toast.error('Продукт не найден!');
                else
                    toast.error('Ошибка!');

                this.fetchData();
            });
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return;
        } else if (!isLoaded) {
            return <Container className="loads">
                <Spinner className="spinner" style={{
                    width: "3rem",
                    height: "3rem"
                }} animation="border" role="status" >
                    <span className="sr-only">Загрузка...</span>
                </Spinner>
            </Container>;
        } else {
            return (
                <Container>
                    <Row className="mb-5 mt-5">
                        <LinkContainer to="/products/edit">
                            <Button variant="primary" size="lg" block>
                                Добавить продукт
                            </Button>
                        </LinkContainer>
                    </Row>
                    <Row>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Название</th>
                                    <th>Стоимость, рублей</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                {items.map((item, index) => (
                                    <tr key={item.id}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.cost}</td>
                                        <td data-key={item.id}>
                                            <LinkContainer to={"/products/edit?id=" + item.id}>
                                                <Button variant="dark" className="mr-2">Редактировать</Button>
                                            </LinkContainer>
                                            <Button variant="danger" onClick={this.deleteItem}>Удалить</Button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Row>
                </Container>
            );
        }
    }
}

export default Products
