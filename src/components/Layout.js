import React, { Component } from 'react';
import { NavMenu } from './NavMenu';
import { ToastContainer } from 'react-toastify';

export class Layout extends Component {
    render() {
        return (
            <div>
                <NavMenu />
                <ToastContainer />
                {this.props.children}
            </div>
        )
    }
}

export default Layout
