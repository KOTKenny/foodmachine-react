import React, { Component } from 'react'
import { Container, Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export class ProductEdit extends Component {

    constructor(props) {
        super(props);
        const queryParams = new URLSearchParams(window.location.search);
        const id = queryParams.get('id');
        this.state = {
            productId: id,
            buttonText: "Создать",
            item: {
                name: "",
                cost: "",
                imgName: "",
            },
            imgPreviewSrc: "",
            file: null
        }
        this.GetData = this.GetData.bind(this);
        this.UpdateData = this.UpdateData.bind(this);
        this.CreateData = this.CreateData.bind(this);
        this.CreateOrUpdateItem = this.CreateOrUpdateItem.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleCostChange = this.handleCostChange.bind(this);
        this.fileHandler = this.fileHandler.bind(this);
        this.UploadFile = this.UploadFile.bind(this);
        this.Save = this.Save.bind(this);
    }

    componentDidMount() {
        if (this.state.productId != null) {
            this.GetData(this.state.productId);
        }
    }

    GetData(id) {
        fetch("../api/product/" + id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        buttonText: "Сохранить",
                        item: {
                            name: result.name,
                            cost: result.cost,
                            imgName: result.imgName
                        },
                        imgPreviewSrc: "../img/" + result.imgName
                    });
                },
                (error) => {

                }
            )
    }

    UploadFile(selectedFile) {
        const formData = new FormData();

        formData.append('File', selectedFile, selectedFile.name);

        fetch(
            "../api/upload/",
            {
                method: 'POST',
                body: formData,
            }
        )
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        item: {
                            ...this.state.item,
                            imgName: result.fileName
                        }
                    }, () => {
                        this.CreateOrUpdateItem();
                    });

                });
                
    }

    UpdateData(id, item) {

        const requestUri = "../api/product/" + id;

        const requestMetadata = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item)
        };

        fetch(requestUri, requestMetadata)
            .then((res) => {
                if (res.status == 204) {
                    toast.success('Продукт обновлён!');
                    this.props.history.push('../products');
                }
                else
                    alert(res.statusText);
            });
    }

    CreateData(item) {

        const requestUri = "../api/product/";

        const requestMetadata = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item)
        };

        fetch(requestUri, requestMetadata)
            .then((res) => {
                if (res.status == 200) {
                    toast.success('Продукт создан!');
                    this.props.history.push('../products');
                }
                else
                    alert(res.statusText);
            });
    }

    fileHandler(e) {

        if (this.checkMimeType(e)) {

            if (e.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    this.setState({
                        imgPreviewSrc: e.target.result
                    })
                }.bind(this);

                reader.readAsDataURL(e.target.files[0]); // convert to base64 string

                this.setState({
                    file: e.target.files[0]
                });
            }
        }
    }

    handleNameChange(event) {
        this.setState({
            item: {
                ...this.state.item,
                name: event.target.value
            }
        });
    }

    handleCostChange(event) {
        this.setState({
            item: {
                ...this.state.item,
                cost: event.target.value
            }
        });
    }

    Save(){

        if(this.checkValidation())
            if(this.state.item.imgName === "")
                this.UploadFile(this.state.file);
            else
                this.CreateOrUpdateItem();
    }

    checkValidation=()=>{
        if(this.state.item.name === ""){
            toast.error('Вы не ввели название!');
            return false;
        }
        if(this.state.item.cost === ""){
            toast.error('Вы не ввели стоимость!');
            return false;
        }
        if(this.state.file == null && this.state.item.imgName === ""){
            toast.error('Вы не выбрали файл!');
            return false;
        }

        return true;
    }

    checkMimeType=(event)=>{

        let files = event.target.files
        let err = []
        const types = ['image/png', 'image/jpeg']
        for(var x = 0; x<files.length; x++) {
            if (types.every(type => files[x].type !== type)) {
            err[x] = files[x].type+' не поддерживаемый формат\n';
          }
        };
        for(var z = 0; z<err.length; z++) { 
            event.target.value = null 
            toast.error(err[z])
        }
       return true;
    }

    CreateOrUpdateItem() {

        if (this.state.productId == null) {
            this.CreateData(this.state.item);
        } else {
            this.UpdateData(this.state.productId, this.state.item);

        }
    }

    render() {
        return (
            <Container className="mt-5">
                <ToastContainer />
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Название</Form.Label>
                        <Form.Control placeholder="Введите название" value={this.state.item.name} onChange={this.handleNameChange} />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Стоимость</Form.Label>
                        <Form.Control placeholder="Введите стоимость" value={this.state.item.cost} onChange={this.handleCostChange} />
                    </Form.Group>
                    <Form.Group>
                        <img id="imgPreview" style={{
                            objectFit: "contain",
                            height: "150px"
                        }} variant="bottom" className="mt-3" src={this.state.imgPreviewSrc} alt="" />
                        <Form.File id="img" label="Выберите изоражение" onChange={this.fileHandler} />
                    </Form.Group>
                    <Button type="button" variant="primary" onClick={this.Save}>
                        {this.state.buttonText}
                    </Button>
                </Form>
            </Container>
        )
    }
}

export default ProductEdit
