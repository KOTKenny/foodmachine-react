import React, { Component } from 'react'
import { Container, Row, Col, Spinner } from 'react-bootstrap'
import { toast } from 'react-toastify';
import PurchaseCard from './controls/PurchaseCard';

export class Purchase extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: 1
            },
            purchases: [],
            error: null,
            isLoaded: false
        }
    }

    componentDidMount() {

        const requestUri = "../api/purchase/user/" + this.state.user.id;

        fetch(requestUri)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        purchases: result
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    }, () => toast.error(`Ошибка: ${this.state.error.message}`));
                }
            )

    }

    render() {
        const { isLoaded, error, purchases } = this.state;
        if (error) {
            return;
        } else if (!isLoaded) {
            return <Container className="loads">
                        <Spinner className="spinner" style={{
                            width: "3rem",
                            height: "3rem"
                        }} animation="border" role="status" >
                            <span className="sr-only">Загрузка...</span>
                        </Spinner>
                    </Container>;
        } else {
            return (
                <Container>
                    <h1 className="text-center mt-2">Мои покупки</h1>
                    <Container className="mt-5">
                        <Row>
                            {purchases.map(p => (
                                <Col key={p.id} className="d-flex justify-content-around mb-2">
                                    <PurchaseCard product={p.product} />
                                </Col>
                            ))}
                        </Row>
                    </Container>
                </Container>
            )
        }
    }
}

export default Purchase
