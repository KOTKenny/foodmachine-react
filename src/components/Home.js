import React, { Component, Suspense } from 'react'
import {Container, Spinner} from 'react-bootstrap';
import { FMUserState } from './controls/FMUserState';

const FMShowcase = React.lazy(() => import('./controls/FMShowcase'))

export class Home extends Component {
    render() {
        return (
            <Container>
                <h1 className="text-center mt-2">Торговый автомат</h1>
            
                <Suspense fallback={
                    <Container className="loads">
                        <Spinner className="spinner" style={{
                            width: "3rem",
                            height: "3rem"
                        }} animation="border" role="status" >
                            <span className="sr-only">Загрузка...</span>
                        </Spinner>
                    </Container>
                }>
                    <FMUserState />
                    <FMShowcase />
                </Suspense>
            </Container>
        );
    }
}

export default Home
