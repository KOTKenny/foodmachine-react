import React, { Component } from 'react';
import { Card, Button } from 'react-bootstrap';
import { toast } from 'react-toastify';

export class MachineCard extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: {
                id: 1
            }
        }
    }

    Buy = (e) =>{

        const requestUri = "../api/product/buy/" + this.props.product.id;

        const requestMetadata = {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(this.state.user)
        }

        fetch(requestUri, requestMetadata)
            .then(res => res.json())
            .then(
                (result) => {
                    toast.success(result.message);
                },
                (error) => {
                    toast.error(error);
                }
            )

    }

    render() {
        return (
            <Card style={{ width: '18rem' }}>
                <div className="glowing">
                    <Card.Img style={{
                        objectFit: "contain",
                        height: "150px"
                    }} variant="bottom" className="mt-3" src={"./img/" + this.props.product.imgName} />
                </div>
                <Card.Body>
                    <Card.Title style={{
                        textAlign: "center"
                    }}>{this.props.product.name}</Card.Title>

                    <Card.Text style={{
                        textAlign: "center"
                    }}>Стоимость: {this.props.product.cost} рубля</Card.Text>

                    <div className="d-flex justify-content-center">
                        <Button variant="primary" onClick={ this.Buy }>Купить</Button>
                    </div>
                </Card.Body>
            </Card>
        );
    }
}

export default MachineCard
