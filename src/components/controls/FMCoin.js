import React, { Component } from 'react'

export class FMCoin extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <li>
                <button type="button" className="coin" data-coins={this.props.nominal} onClick={(e) => this.props.onCoinClick(e)}>
                    <div className="coin-text" data-coins={this.props.nominal}>{this.props.nominal}</div>
                </button>
            </li>
        )
    }
}

export default FMCoin
