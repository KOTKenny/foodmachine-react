import React, { Component } from 'react'
import { Container, Card, Button, Spinner } from 'react-bootstrap'
import { FMCoin } from './FMCoin';
import { toast } from 'react-toastify';

export class FMUserState extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sum: 0,
            coins: [],
            user: {
                Id: 1
            },
            isWithdrawalClicked: false,
        };

        this.fetchCoins = this.fetchCoins.bind(this);
    }

    fetchCoins(){

        fetch("api/coin")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        coins: result
                    });
                })
        
        const requestUri = "api/user/" + this.state.user.Id

        fetch(requestUri)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        sum: result.coinsInFM
                    });
                })

    }

    componentDidMount(){
        this.fetchCoins();
    }

    Withdrawal = () => {

        this.setState({ isWithdrawalClicked: true });

        const requestUri = "api/machine/withdrawal";

        const requestMetadata = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.user)
        }

        fetch(requestUri, requestMetadata)
            .then(
                (res) => {
                    if(res.status == 200){
                        toast.success(`Средства выведены`);
                        this.setState({
                            sum: 0
                        });
                    }else if(res.status == 304){
                        toast.warning(`В данный момент автомат не может выдать нужную сумму. Приносим извинения за предоставленные неудобства`);
                    }else if(res.status == 500){
                        toast.error(`Server Error`);
                    }
                }
            )
            .then(() => this.setState({
                isWithdrawalClicked: false
            }))
    
    }

    AddCoins = (e) => {

        let coinNominal = parseInt(e.target.getAttribute("data-coins"))

        const requestUri = "api/machine/addcoin/" + coinNominal;

        const requestMetadata = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.user)
        };

        fetch(requestUri, requestMetadata)
            .then((res) =>{
                if(res.status == 200){
                    this.setState({
                        sum: this.state.sum + coinNominal
                    })
                }else{
                    toast.error(`${res.body}`);
                }
            })

    }

    render() {
        const { coins, isWithdrawalClicked } = this.state;
        return (
            <Container>
                <Card>
                    <Card.Header as="h5">Состояние автомата</Card.Header>
                    <Card.Body>
                        <Card.Text>
                            В автомате: {this.state.sum} рублей
                        </Card.Text>
                        <Card.Text>
                            Добавить монет в автомат
                        </Card.Text>
                        <div className="coin-list">
                            <ul>
                                {coins.map((coin) => (
                                    <FMCoin key={coin.id} nominal={coin.nominal} onCoinClick={this.AddCoins} />
                                ))}

                            </ul>
                        </div>
                        {isWithdrawalClicked ? (
                            <Button variant="primary" disabled>
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                Загрузка
                            </Button>) : (
                                <Button variant="primary" onClick={ this.Withdrawal }>Запросить вывод</Button>
                            )}
                    </Card.Body>
                </Card>
            </Container>
        )
    }
}

export default FMUserState
