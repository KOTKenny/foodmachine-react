import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

export class PurchaseCard extends Component {
    render() {
        return (
            <Card style={{ width: '18rem' }}>
                <div className="glowing">
                    <Card.Img style={{
                        objectFit: "contain",
                        height: "150px"
                    }} variant="bottom" className="mt-3" src={"./img/" + this.props.product.imgName} />
                </div>
                <Card.Body>
                    <Card.Title style={{
                        textAlign: "center"
                    }}>{this.props.product.name}</Card.Title>
                </Card.Body>
            </Card>
        )
    }
}

export default PurchaseCard
