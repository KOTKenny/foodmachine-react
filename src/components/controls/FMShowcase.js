import React, { Component } from 'react';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import { MachineCard } from './MachineCard';
import { toast } from 'react-toastify'

export class FMShowcase extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("api/product")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    }, () => toast.error(`Ошибка: ${this.state.error.message}`));
                }
            );

    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return;
        } else if (!isLoaded) {
            return <Container className="loads">
                        <Spinner className="spinner" style={{
                            width: "3rem",
                            height: "3rem"
                        }} animation="border" role="status" >
                            <span className="sr-only">Загрузка...</span>
                        </Spinner>
                    </Container>;
        } else {
            return (
                <Container className="mt-5">
                    <Row className="mb-2">
                        {items.map(item => (
                            <Col key={item.id} className="d-flex justify-content-around mb-2">
                                <MachineCard product={item} />
                            </Col>
                        ))}

                    </Row>
                </Container>
            )
        }
    }
}

export default FMShowcase;
