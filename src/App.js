import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Purchase } from './components/Purchase';
import { Home } from './components/Home';
import { Layout } from './components/Layout'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Products from './components/Products';
import ProductEdit from './components/ProductEdit';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/purchase' component={Purchase} />
        <Route exact path='/products' component={Products} />
        <Route exact path='/products/edit' component={ProductEdit} />
      </Layout>
    );
  }
}